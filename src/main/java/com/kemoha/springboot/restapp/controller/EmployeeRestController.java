package com.kemoha.springboot.restapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kemoha.springboot.restapp.entity.Employee;
import com.kemoha.springboot.restapp.service.EmployeeService;

@RestController
@RequestMapping("/api")
public class EmployeeRestController {

	private EmployeeService employeeService;

	// Construction Injection
	@Autowired
	public EmployeeRestController(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	// set endpoint for the list of all the employees
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {

		return employeeService.findAll();
	}

	// set endpoint for the employee with id
	@GetMapping("/employees/{id}")
	public Employee getEmployee(@PathVariable int id) {
		Employee emp = employeeService.findById(id);

		if (null == emp) {

			throw new RuntimeException("Not found" + id);
		}
		return emp;
	}
	
	//end point for adding a new employee
	@PostMapping("/employees")
	public Employee saveEmployee(@RequestBody Employee emp) {

		employeeService.save(emp);

		return emp;
	}
	
	//end point for updating an existing employee
	@PutMapping("/employees")
	public Employee updateEmployee(@RequestBody Employee emp) {

		employeeService.save(emp);

		return emp;
	}
	
	
	//end point for deleting employee 
	@DeleteMapping("/employees/{id}")
	public void deleteEmployee(@PathVariable int id) {

		Employee emp = employeeService.findById(id);

		if (null == emp) {

			throw new RuntimeException("Not found" + id);
		} else {
			employeeService.deleteById(id);
		}

	}
}
