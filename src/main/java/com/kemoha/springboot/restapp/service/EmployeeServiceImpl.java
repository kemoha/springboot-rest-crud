package com.kemoha.springboot.restapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kemoha.springboot.restapp.dao.EmployeeDAO;
import com.kemoha.springboot.restapp.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Override
	@Transactional
	public List<Employee> findAll() {
		List<Employee> employees = employeeDAO.findAll();
		return employees;
	}

	@Override
	@Transactional
	public Employee findById(int id) {
		Employee employee = employeeDAO.findById(id);
		return employee;
	}

	@Override
	@Transactional
	public void save(Employee emp) {
		employeeDAO.save(emp);

	}

	@Override
	@Transactional
	public void deleteById(int id) {
		employeeDAO.deleteById(id);

	}

}
