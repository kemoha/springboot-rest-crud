package com.kemoha.springboot.restapp.service;

import java.util.List;

import com.kemoha.springboot.restapp.entity.Employee;

public interface EmployeeService {
	
	public List<Employee> findAll();
	
	public Employee findById(int id);
	
	public void save(Employee emp);
	
	public void deleteById(int id);

}
