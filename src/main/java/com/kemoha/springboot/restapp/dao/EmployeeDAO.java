package com.kemoha.springboot.restapp.dao;

import java.util.List;

import com.kemoha.springboot.restapp.entity.Employee;

public interface EmployeeDAO {
	
	public List<Employee> findAll();
	
	public Employee findById(int id);
	
	public void save(Employee emp);
	
	public void deleteById(int id);

}
