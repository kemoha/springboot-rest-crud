package com.kemoha.springboot.restapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kemoha.springboot.restapp.entity.Employee;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

	// define field for Entity Manager
	private EntityManager entityManager;

	// set up for construction injection
	@Autowired
	public EmployeeDAOImpl(EntityManager entityMgr) {
		this.entityManager = entityMgr;
	}

	@Override
	@Transactional
	public List<Employee> findAll() {

		// GEt the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);

		//create query
		Query<Employee> query = currentSession.createQuery("from Employee", Employee.class);
		
		// execute query and get the result list
		List<Employee> employeeList = query.getResultList();
		
		// return the results
		return employeeList;
	}
	
	
	@Override
	@Transactional
	public Employee findById(int id) {
		
		// GEt the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		Employee employee = currentSession.get(Employee.class, id);
	
		return employee;
	}
	
	@Override
	@Transactional
	public void save(Employee emp) {
		
		// GEt the current hibernate session
				Session currentSession = entityManager.unwrap(Session.class);
				
				currentSession.saveOrUpdate(emp);
		
				
	}
	
	@Override
	@Transactional
	public void deleteById(int id) {
		
		// GEt the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		Employee employee = currentSession.get(Employee.class, id);
		
		currentSession.delete(employee);
		
		
	}

}
